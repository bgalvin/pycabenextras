#!/usr/bin/python2

"""Relaxation Registration"""
import matplotlib

#matplotlib.use('Agg')
import matplotlib.pyplot as plt

from AppUtils import Config, Optim, Compute

from PyCA.Core import *
import PyCA.Common as common
import PyCA.Display as display

#except:
#    print "Warning: matplotlib import failed, some functionality disabled"

import os, sys, errno

StudySpec = {
	'I0':
		Config.Param(default='subject0.mhd', required=True,
					 comment="Initial (moving) image file"),
	'I1':
		Config.Param(default='subject1.mhd', required=True,
					 comment="Target (fixed) image file")}
RelaxSpec = {
	'fluidParams':
		Config.Param(default=[0.01, 0.01, 0.001], required=False,
					 comment="Parameters for diffop kernel"),
	'smoothingConstant':
		Config.Param(default=0.2, required=False,
					 comment="Vector field smoothing constant"),
	'numb_V':
		Config.Param(default=4, required=False,
					 comment="Number of velocity fields")
}

MatchingConfigSpec = {
	'study': StudySpec,
	'optim': Optim.OptimConfigSpec,
	'relax': RelaxSpec,
	'_resource': 'Relaxation Reg'}


def RelaxationReg(cf):

	# choose diffOp and memType based off of what we are running on
	if GetNumberOfCUDADevices() > 0:
		print "Found " + str(GetNumberOfCUDADevices()) + " CUDA device(s)."
		mType = MEM_DEVICE
		diffOp = FluidKernelFFTGPU()
	else:
		print "No CUDA devices detected!"
		mType = MEM_HOST
		diffOp = FluidKernelFFTCPU()

	
	

	#load up the images and set up grid
	I0img = common.LoadPNGImage(cf.study.I0, mType)
	I1img = common.LoadPNGImage(cf.study.I1, mType)
	grid = I0img.grid()

	# pull variables from the config file
	nIter = cf.optim.Niter
	stepSize = cf.optim.stepSize
	fluidParams = cf.relax.fluidParams
	smoothingConstant = cf.relax.smoothingConstant
	N = cf.relax.numb_V

	#set the fluid params for the diffOp kernel
	diffOp.setAlpha(fluidParams[0])
	diffOp.setBeta(fluidParams[1])
	diffOp.setGamma(fluidParams[2])
	diffOp.setGrid(grid)

	#ini vars
       	J = []
	v = []
	phi_f = []


	#ini lists to hold our def fields and image
	#J = [SetMem(Image3D(grid, mType), 0) for i in range(N+1)]
	#phi_f = [SetMem(Image3D(grid, mType), 0) for i in range(N+1)]
	#v = [SetToIdentity(Field3D(grid, mType)) for i in range(N)]
	
	for i in range(N+1):
		temp_j = Image3D(grid, mType)
		SetMem(temp_j, 0.0)
		J.append(temp_j)
		
		temp_v = Field3D(grid, mType)
		SetToIdentity(temp_v)
		phi_f.append(temp_v)
		
	for i in range(N):
		
		temp_v = Field3D(grid, mType)
		SetMem(temp_v, 0.0)
		v.append(temp_v)

	

	# allocate vars and make sure that the memory is clear!
	I0 = Image3D(grid, mType)
	SetMem(I0, 0.0)

	I1 = Image3D(grid, mType)
	SetMem(I1, 0.0)

	h = Field3D(grid, mType)
	SetToIdentity(h)

	deformedImg = Image3D(grid, mType)
	SetMem(deformedImg, 0.0)

	diff = Image3D(grid, mType)
	SetMem(diff, 0.0)

	gradI = Field3D(grid, mType)
	#SetToIdentity(gradI)
	SetMem(gradI, 0.0)
	
	gU = Field3D(grid, mType)
	#SetToIdentity(gU)
	SetMem(gU, 0.0)

	scratch = Field3D(grid, mType)
	SetToIdentity(scratch)
	SetMem(scratch, 0.0)

	scratch1 = Field3D(grid, mType)
	SetToIdentity(scratch1)
	SetMem(scratch1, 0.0)
	
	scratch2 = Field3D(grid, mType)
	SetToIdentity(scratch2)	
	SetMem(scratch2, 0.0)

	scratch3 = Field3D(grid, mType)
	SetToIdentity(scratch3)
	

	h_scratch = Field3D(grid, mType)
	SetToIdentity(h_scratch)

	#cp images over to working variables
	Copy(I1, I1img)
	Copy(I0, I0img)
	Copy(J[0], I0img)

	energy = []
	
	#the gradient is computed at time t and multiplied by the splatted diff to that time

	#iterate until nIter (from config)
	for it in range(nIter):
		 
		for t in range(N+1):
			#reset for next timepoint
			SetToIdentity(h)

			#to get a deformation to time t we start at time t and integrate backwards
			for t_inside in reversed(range(t)):
				#reset h_scratch, 
				SetToIdentity(h_scratch)
				#compose h_scrath -v[t_inside](h_scratch).  t_inside is moving towards 0
				ComposeVInvH(h_scratch,v[t_inside],h)
				Copy(h, h_scratch)			
			
			#copy the h feild into phi[t] and apply the h feild to I0 to get forward deformed images
			Copy(phi_f[t],h)	
			ApplyH(J[t], I0, h)

		#diff at final time point
		Sub(diff,J[N],I1)	
		
                #reset h for the next section
		SetToIdentity(h)
		
		for t in range(N):
		
			#Gradient at time t
			Gradient(gradI,J[t+1])
			
			#Splat the diff image to time t 
			Splat(deformedImg,phi_f[N-t],diff)			


			# multiply the gradient and the splatted diff
			Mul(scratch,gradI,deformedImg)

			#apply DiffOP to the product from before and multiply that times the smoothing constant
			diffOp.applyInverseOperator(gU,scratch)
			
			#\lambda*K(...)
			MulC(scratch1,gU,smoothingConstant)

			#Grad decent for each V

			#scratch2 = 2v_t-_lambda*K(...)
			MulCSub(scratch2,v[t],2,scratch1)
			Add_MulC(scratch3,v[t],scratch2,-stepSize)
			Copy(v[t],scratch3)
		
		energy.append(Sum2(diff))
		print 'iter: %d' % it + ", energy: " + str(Sum2(diff))

	plt.figure('results', figsize=(12, 10))
	plt.clf()
	plt.subplot(2, 3, 1)
	display.DispImage(J[0], 'j0 (aka I0)', newFig=False)
	plt.subplot(2, 3, 2)
	display.DispImage(J[N], 'j['+str(N)+']', newFig=False)
	plt.subplot(2, 3, 3)
	display.DispImage(I1, 'I1', newFig=False)
	plt.subplot(2, 3, 4)
	display.DispImage(diff, 'diff', newFig=False)
	plt.subplot(2, 3, 5)
	display.GridPlot(phi_f[N], every=5)
	#display.GridPlot
	plt.title('h (def field)')
	plt.subplot(2, 3, 6)
	plt.plot(energy)
	plt.title('energy')
	plt.xlabel("iterations")
	plt.draw()
	plt.savefig('results_relax.png')
	plt.show()

	raw_input("Press Enter to continue...")
	return

if __name__ == '__main__':
	try:
		usercfg = Config.Load(spec=MatchingConfigSpec, argv=sys.argv)
	except Config.MissingConfigError:
		sys.exit(1)

	RelaxationReg(usercfg)

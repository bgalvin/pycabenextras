#!/usr/bin/python2

"""Elastic Registration"""
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


from AppUtils import Config, Optim, Compute

from PyCA.Core import *
import PyCA.Common as common
import PyCA.Display as display

#try:

#except:
#    print "Warning: matplotlib import failed, some functionality disabled"

import os, sys, errno

StudySpec = {
    'I0':
        Config.Param(default='subject0.mhd', required=True,
            comment="Initial (moving) image file"),
    'I1':
        Config.Param(default='subject1.mhd', required=True,
            comment="Target (fixed) image file")}
ElastSpec = {
    'fluidParams':
        Config.Param(default='[0.5, 0.5, 0.001]', required=False,
            comment="Parameters for diffop kernel"),
    'smoothingConstant':
        Config.Param(default='0.2', required=False,
            comment="Vector field smoothing constant")
}
MatchingConfigSpec = {
    'study': StudySpec,
    'optim': Optim.OptimConfigSpec,
    'elast': ElastSpec,
    '_resource': 'Elastic Reg'}

def elastReg(cf):


	# choose diffOp based off of what we are running on
	if GetNumberOfCUDADevices() > 0:
		mType = MEM_DEVICE
		diffOp = FluidKernelFFTGPU()
	else:
		print "No CUDA devices detected!"
		mType = MEM_HOST
		diffOp = FluidKernelFFTCPU()

	#load up the images and set up grid
	I0img = common.LoadPNGImage(cf.study.I0, mType)
	I1img = common.LoadPNGImage(cf.study.I1, mType)

	grid = I0img.grid()

	# pull variables from the config file
	nIter = cf.optim.Niter
	stepSize = cf.optim.stepSize
	fluidParams = cf.elast.fluidParams
	smoothingConstant = cf.elast.smoothingConstant

	#set the fluid params for the diffOp kernel
	diffOp.setAlpha(fluidParams[0])
	diffOp.setBeta(fluidParams[1])
	diffOp.setGamma(fluidParams[2])
	diffOp.setGrid(grid)

	# allocate vars and make sure that the memory is clear!
	I0 = Image3D(grid, mType)
	SetMem(I0, 0.0)

	I1 = Image3D(grid, mType)
	SetMem(I1, 0.0)

	u = Field3D(grid, mType)
	SetMem(u, 0.0)

	deformedGrad = Field3D(grid, mType)
	SetMem(deformedGrad, 0.0)

	deformedImg = Image3D(grid, mType)
	SetMem(deformedImg, 0.0)

	diff = Image3D(grid, mType)
	SetMem(diff, 0.0)

	gI = Field3D(grid, mType)
	SetMem(gI, 0.0)

	gU = Field3D(grid, mType)
	SetMem(gU, 0.0)
	
	u_store= Field3D(grid, mType)
	SetMem(u_store, 0.0)

	#cp images over to working variables
	Copy(I0, I0img)
	Copy(I1, I1img)
	SetToIdentity(u)
	
	energy = []

	for it in range(nIter):

		# The displacements are a smoothed version of a difference
		# image multiplied by the warped gradients of the template

		# apply current def to I0 ~ deformedImg = I_0 \circ u
		ApplyH(deformedImg, I0, u)
		
		#Gradiant of deformed image ~ gI = \nabla I_0 \circ u
		Gradient(gI, deformedImg)
		
		# apply def to gradient image  ~ deformedGrad = \Delta I_0 \circ u
		#ApplyV(deformedGrad, gI, u, BACKGROUND_STRATEGY_CLAMP)

		# calc difference between where we are are our goal ~ diff = I_1- I_0 \circ u
		diff = I1-deformedImg

		# combine the difference image and the warped gradients and store in scratch ~ gI = gI*diff
		gI*=diff
		# apply the diffOp ~ K[ (\Delta I_0 \circ u)*(I_1- I_0 \circ u ) ]
		diffOp.applyInverseOperator(gU, gI)
		
		
		gU*=stepSize
		
		ApplyV(u_store,u,gU,BACKGROUND_STRATEGY_PARTIAL_ID)
		#ComposeHV(u_store, u, gU)
		
		# u = u_store
		u.swap(u_store)

		energy.append(Sum2(diff))
		
		print 'iter: %d'%it + ", energy: " + str( Sum2(diff))


	plt.figure('results',figsize=(12, 8))
	plt.clf()
	plt.subplot(2,3,1)
	display.DispImage(I0, 'initial (moving) image', newFig=False)
	plt.subplot(2,3,2)
	display.DispImage(I1, 'target (fixed) image', newFig=False)
	plt.subplot(2,3,3)
	display.DispImage(deformedImg, 'u(I0)', newFig=False)
	plt.subplot(2,3,4)
	display.DispImage(diff, 'difference image', newFig=False)
	plt.subplot(2,3,5)
	display.GridPlot(u,every=5,isVF=True)
	display.GridPlot
	plt.title('h (def field)')
	plt.subplot(2,3,6)
	plt.plot(energy)
	plt.title('energy')
	plt.xlabel("iterations")
	plt.draw()
	plt.savefig('results_greedy2.png')
	#plt.show()

	#raw_input("Press Enter to continue...")
	return

if __name__ == '__main__':
    try:
        usercfg = Config.Load(spec=MatchingConfigSpec, argv=sys.argv)
    except Config.MissingConfigError:
        sys.exit(1)

    elastReg(usercfg)